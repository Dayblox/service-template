import express from "express"
import { dispatchToken } from "./config"

const app = express()

app.get("/", function (req, res) {
    res.send("Hello World!")
})

app.listen(8080, function () {
    console.log("Listening on port 8080!")
})